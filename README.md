# Room Service Spring Boot

A small Spring Boot Project based on a 3 layered architecture:
- Presentation Layer --> @RestController
- Service Layer --> @Service
- Repository Layer --> @Repository
Postman used to send and receive requests, POST data to the service.