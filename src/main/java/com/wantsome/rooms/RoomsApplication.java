package com.wantsome.rooms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/*
 The entry point of the spring boot application is the class which contains
       - @SpringBootApplication annotation and
       - the main method.

@SpringBootApplication annotation includes
    - Auto- Configuration,
    - Component Scan, and
    - Spring Boot Configuration
 */

@ComponentScan(basePackages = {"com.wantsome.rooms"})
@EnableJpaRepositories(basePackages = "com.wantsome.rooms")
@EntityScan("com.wantsome.rooms.model")
@SpringBootApplication
public class RoomsApplication {

    public static void main(String[] args) {
        SpringApplication.run(RoomsApplication.class, args);

    }
}
