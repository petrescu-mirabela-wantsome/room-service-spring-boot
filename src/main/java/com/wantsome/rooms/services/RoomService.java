package com.wantsome.rooms.services;

import com.wantsome.rooms.model.Room;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface RoomService {

    String hello();

    Room addRoom(Room room);

    List<Room> addMultipleRooms(List<Room> roomList);

    Room getRoomId(String id);

    Iterable<Room> getAllRoom();

    Optional<Room> updateRoom(Room room);

    void deleteRoomById(String id);
}
