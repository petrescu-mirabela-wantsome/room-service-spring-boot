-- drop TABLE rooms; 

CREATE TABLE rooms
(
    id character(200) NOT NULL PRIMARY KEY,
    capacity numeric,
    floor numeric,
    name character(50),
    description character(200)

);

select * from rooms;